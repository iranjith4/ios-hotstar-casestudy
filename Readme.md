#### How to run the project

- Clone the project
- Open the `ios-hotstar-casestudy.xcodeproject` and Run the project in a simulator.
- For receptionist - use `reception` as Username and `1234` as password.
- For Customer, once the Customer is added to the Queue - use the `PhoneNumber` as Login and Password is `1234`


### Things Implemented

 - Customer and Receptionist Login
 - Used `UserDefaults` for maintaining the login, and used `CoreData` for maintaining the Customer and booking details
 - Used MVVM Pattern, Singleton
 - Used both Storyboard and Code for Designing the UIViews
 - Used `Swift 4.0, Xcode 9.2`


 ### Workflow
 - Once the Receptionist logged in, he can add user to the queue.
 - Duplicate users cannot be added. Dupilcate is identified by phone number.
 - At anypoint of time, receptionist can remove any user from the table - Assuming the customer has finished the dining.
 - Receptionist can only move the first customer in the Queue to the table. The customer will be allocated to the free table.
 - Customer can login and check his average time.
 - Customers who is in the table, or finished the dining cant login.

 ### Improvements
 - Currently phone number field takes all the input. SHould limit to only numbers.
 - Change Keypad types for the respective fields.

 ### Screenshots
 ![Login](https://raw.githubusercontent.com/iranjith4/ios-hotstar-casestudy/master/screenshots/1.png)
 ![Waitlist](https://raw.githubusercontent.com/iranjith4/ios-hotstar-casestudy/master/screenshots/2.png)
 ![Add Customer](https://raw.githubusercontent.com/iranjith4/ios-hotstar-casestudy/master/screenshots/3.png)
 ![Customer](https://raw.githubusercontent.com/iranjith4/ios-hotstar-casestudy/master/screenshots/4.png)


Thanks for visiting my repo. For help email at r@ir4.in
