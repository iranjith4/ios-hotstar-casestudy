//
//  HSWaitlistModel.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class HSWaitlistModel: NSObject {
    
    private let totalTables : Int = 8
    private let tableSections = ["Allocated","Queue"]
    private var allocatedList = [Customer]()
    private var queuedList = [Customer]()
    
    func numberOfSections() -> Int{
        return tableSections.count
    }
    
    func numberOfRows(forSection section : Int) -> Int {
        if section == 0 {
            return allocatedList.count
        }else if section == 1 {
            return queuedList.count
        }
        return 0
    }
    
    func getCustomer(forIndexPath indexPath : IndexPath) -> Customer! {
        if indexPath.section == 0 {
            return allocatedList[indexPath.row]
        }else if indexPath.section == 1 {
            return queuedList[indexPath.row]
        }
        return nil
    }
    
    func title(forSection section : Int) -> String{
        if section < tableSections.count {
            return tableSections[section]
        }
        return ""
    }
    
    func totalNumberOfTables() -> String {
        return "\(totalTables)"
    }
    
    func occupiedTables() -> String {
        return "\(allocatedList.count)"
    }
    
    func waitingQueueCount() -> String {
        return "\(queuedList.count)"
    }
    
    func loadCustomers(completion : () -> ()) {
        if let customers = HSCoreDataManager().getAllCustomers() {
            let allocatedList = customers.filter({$0.status == 1})
            self.allocatedList = allocatedList.sorted(by: {$0.table < $1.table})
            let queuedList = customers.filter({$0.status == 0})
            self.queuedList = queuedList
        }
        completion()
    }
    
    private func getFirstEmptyTable() -> Int {
        var filledTables = [Int]()
        for customer in allocatedList {
            filledTables.append(Int(customer.table))
        }
        
        for table in 1...totalTables {
            if filledTables.contains(table) == false {
                print("Not Filled : \(table)")
                return table
            }
        }
        
        return 1
    }
    
    func processQueue(completion : (Bool) -> ()) {
        if self.allocatedList.count < totalTables {
            if self.queuedList.count > 0 {
                let customerToAllocate = self.queuedList.first!
                setCustomerStatus(status: .allocated, forCustomer: customerToAllocate, table: getFirstEmptyTable(), completion: {
                    completion(false)
                })
            }else {
                completion(false)
            }
        }else {
            completion(true)
        }
    }
    
    private func setCustomerStatus( status : HSUtility.UserStatus, forCustomer customer : Customer, table : Int, completion : () -> ()) {
        HSCoreDataManager().setCustomerStatus(status: status, customerPhone: customer.phone!, table : table)
        loadCustomers {
            completion()
        }
    }
    
    func customerFinished(at index : Int, completion : () -> ()) {
        let finishedCustomer = allocatedList[index]
        setCustomerStatus(status: .finished, forCustomer: finishedCustomer, table: 0) {
            completion()
        }
    }
    
    func getQueueNumber(forCustomer customer : Customer) -> Int {
        if let queuedCustomer = queuedList.filter({$0.phone == customer.phone}).first {
            return queuedList.index(of: queuedCustomer)!
        }else {
            return 0
        }
    }
}
