//
//  HSLoginModel.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class HSLoginModel: NSObject {
    
    //Hardcoded Users
    let receptionist1 = (username : "reception", password : "1234")
    let customerPassword = "1234"
    
    var userType : HSUtility.UserType = .customer
    
    func authenticateLogin(withUserName username : String, andPassword password : String) -> Bool{
        if userType == .customer {
            return false
        }else if userType == .receptionist {
            if username == receptionist1.username && password == receptionist1.password {
                return true
            }
        }
        return false
    }
    
    func authenticateCustomer(withUserName username : String, andPassword password : String) -> Bool {
        if HSCoreDataManager().doesThisUserExist(phoneNumber: username) && password == customerPassword {
            return true
        }
        return false
    }
    
    func getCustomer(forPhoneNumber phoneNumber : String) -> Customer! {
        let customer = HSCoreDataManager().getCustomer(forPhone: phoneNumber)
        if customer != nil && customer?.table == 0 {
            return customer
        }else {
            return nil
        }
    }

}
