//
//  HSCoreDataManager.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit
import CoreData

class HSCoreDataManager: NSObject {
    
    enum EntityName : String {
        case customer = "Customer"
    }
    
    private func saveData(entityName : String, data : [String : Any]) {
        guard let del = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = del.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: managedContext)
        let model = NSManagedObject.init(entity: entity!, insertInto: managedContext)
        
        for key in data.keys {
            model.setValue(data[key], forKey: key)
        }
        do {
            try managedContext.save()
        } catch let er {
            print("Error when Saving Data",er)
        }
    }
    
    private func getData(entity : String) ->  [NSFetchRequestResult]! {
        guard let del = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        
        let managedContext = del.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSManagedObject>(entityName: entity)
        do {
            let data = try managedContext.fetch(fetch)
            return data
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
    
    private func getDataWithPredicate (entity : String, predicate : NSPredicate) ->  [NSFetchRequestResult]!{
        guard let del = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        
        let managedContext = del.persistentContainer.viewContext
        let fetch = NSFetchRequest<NSManagedObject>(entityName: entity)
        fetch.predicate = predicate
        do {
            let data = try managedContext.fetch(fetch)
            return data
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
    
    //Save a Customer
    func saveAddedCustomer(customer : [String : Any]) {
        self.saveData(entityName: EntityName.customer.rawValue, data: customer)
    }
    
    //Save a Customer
    func getAllCustomers() -> [Customer]! {
        if let customers = getData(entity: EntityName.customer.rawValue) {
            return (customers as? [Customer])!
        }
        return nil
    }
    
    func doesThisUserExist(phoneNumber : String) -> Bool {
        let predicate = NSPredicate(format: "phone = %@", phoneNumber)
        if let customers = getDataWithPredicate(entity: EntityName.customer.rawValue, predicate: predicate) {
            if customers.count > 0 {
                return true
            }else {
                return false
            }
        }
        return false
    }
    
    func getCustomer(forPhone number : String) -> Customer! {
        let predicate = NSPredicate(format: "phone = %@", number)
        if let customers = getDataWithPredicate(entity: EntityName.customer.rawValue, predicate: predicate) as? [Customer] {
            if customers.count > 0 {
                return customers.first
            }
        }
        return nil
    }
    
    func setCustomerStatus(status : HSUtility.UserStatus, customerPhone phone : String, table : Int) {
        guard let del = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = del.persistentContainer.viewContext
        let predicate = NSPredicate(format: "phone = %@", phone)
        if let customers = getDataWithPredicate(entity: EntityName.customer.rawValue, predicate: predicate) as? [Customer] {
            if let customer = customers.first {
                if status == .allocated || status == .queued {
                    customer.status = Int16(status.rawValue)
                    customer.table = Int16(table)
                } else if status == .finished {
                    managedContext.delete(customer)
                }
                do {
                    try managedContext.save()
                }catch {
                    print("Save Error")
                }
            }
        }
    }
}
