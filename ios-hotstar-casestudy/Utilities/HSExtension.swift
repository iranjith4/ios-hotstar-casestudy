//
//  HSExtension.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

extension UIColor {
    static let themeGreen = #colorLiteral(red: 0.5442900062, green: 0.7350739837, blue: 0.2940081358, alpha: 1)
    static let themeGray = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
}
