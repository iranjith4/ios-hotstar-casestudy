//
//  HSUtility.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class HSUtility: NSObject {
    
    static let shared = HSUtility()
    
    enum UserType {
        case receptionist
        case customer
    }
    
    enum UserStatus : Int {
        case queued = 0
        case allocated = 1
        case finished = 2
    }
    
    enum StoryboardId : String {
        case loginController = "HSLoginController"
        case waitingListController = "HSWaitingListController"
        case addQueueController = "HSAddQueueController"
        case customerController = "HSCustomerViewController"
    }
    
    enum UserDefaultsKey : String {
        case isLoggedIn = "HS_UD_ISLOGGEDIN"
        case customerId = "HS_UD_CUSTOMERID"
        case userType = "HS_UD_USERTYPE"
    }
    
    enum NotificationKey : String {
        case logout = "HS_LOGOUT"
    }
    
    class func getStoryBoard(forId storyboardId : String) -> UIViewController  {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: storyboardId)
    }

    class func showAlert(withTitle title : String, message : String, showOnController controller : UIViewController) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: .cancel, handler: nil))
        controller.present(alert, animated: true, completion: nil)
        
    }
    
    class func getNavigationButtonWith(buttonTitle name : String, navigationButtons : (UIBarButtonItem, UIButton) -> ()) {
        let button = UIButton.init()
        button.setTitle(name, for: .normal)
        button.setTitleColor(UIColor.themeGreen, for: .normal)
        let barButton = UIBarButtonItem.init(customView: button)
        navigationButtons(barButton, button)
    }
    
    /* UserDefaults for saving local data */
    
    func saveUDStringData(value : String, for key : String) {
        let ud = UserDefaults.standard
        ud.set(value, forKey: key)
    }
    
    func getUDStringData(for key : String) -> String!{
        let ud = UserDefaults.standard
        guard let value = ud.value(forKey: key) as? String else {
            return nil
        }
        return value
    }
    
    func saveUDIntData(value : Int, for key : String) {
        let ud = UserDefaults.standard
        ud.set(value, forKey: key)
    }
    
    func saveUDBoolData(value : Bool, for key : String) {
        let ud = UserDefaults.standard
        ud.set(value, forKey: key)
    }

    func getUDIntData(for key : String) -> Int!{
        let ud = UserDefaults.standard
        guard let value = ud.value(forKey: key) as? Int else {
            return nil
        }
        return value
    }
    
    func getUDBoolData(for key : String) -> Bool!{
        let ud = UserDefaults.standard
        guard let value = ud.value(forKey: key) as? Bool else {
            return nil
        }
        return value
    }
    
    func clearAllUD() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
        defaults.synchronize()
    }
}
