//
//  HSWaitlistView.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class HSWaitlistView: UIView {
    
    let waitlistBanner = HSWaitlistBanner()
    let waitlistTable = UITableView.init(frame: CGRect.init(), style: .plain)

    override var frame: CGRect {
        didSet {
            updateView()
        }
    }
    
    init() {
        super.init(frame: CGRect.init())
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        //All the custom initialisation goes here
        waitlistTable.register(HSCustomerCell.self, forCellReuseIdentifier: HSCustomerCell.reuseIdentifier)
        self.addSubview(waitlistBanner)
        self.addSubview(waitlistTable)
        updateView()
    }
    
    func updateView() {
        waitlistBanner.frame = CGRect.init(origin: CGPoint.init(), size: CGSize.init(width: frame.size.width, height: 100))
        waitlistTable.frame = CGRect.init(origin: CGPoint.init(x: 0, y: waitlistBanner.frame.size.height), size: CGSize.init(width: self.frame.size.width, height: self.frame.size.height - (waitlistBanner.frame.origin.y + waitlistBanner.frame.size.height)))
    }

}
