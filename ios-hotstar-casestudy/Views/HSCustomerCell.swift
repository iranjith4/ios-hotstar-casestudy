//
//  HSCustomerCell.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 01/03/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class HSCustomerCell: UITableViewCell {
    
    static let reuseIdentifier = "HSCustomerCell"
    static let _cellHeight : CGFloat = 70
    let cellHeight = HSCustomerCell._cellHeight
    
    let customerName = UILabel()
    let customerPhone = UILabel()
    let numberOfPeopleLabel = UILabel()
    let tableNumberLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell() {
        customerName.textColor = UIColor.black
        customerName.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        
        customerPhone.textColor = UIColor.lightGray
        customerPhone.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        
        numberOfPeopleLabel.textColor = UIColor.lightGray
        numberOfPeopleLabel.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        
        tableNumberLabel.textColor = UIColor.themeGreen
        tableNumberLabel.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        tableNumberLabel.textAlignment = .right
        
        
        self.addSubview(customerName)
        self.addSubview(customerPhone)
        self.addSubview(numberOfPeopleLabel)
        self.addSubview(tableNumberLabel)
    }
    
    func updateCell() {
        let labelSize = CGSize.init(width: self.frame.size.width - 68, height: cellHeight / 3)
        var yPos : CGFloat  = 0
        var xPos : CGFloat  = 16
        customerName.frame = CGRect.init(origin: CGPoint.init(x: xPos, y: yPos), size: labelSize)
        yPos += customerName.frame.size.height
        
        customerPhone.frame = CGRect.init(origin: CGPoint.init(x: xPos, y: yPos), size: labelSize)
        yPos += customerPhone.frame.size.height
        
        numberOfPeopleLabel.frame = CGRect.init(origin: CGPoint.init(x: xPos, y: yPos), size: labelSize)
        yPos += numberOfPeopleLabel.frame.size.height
        
        xPos = self.frame.size.width - 68
        tableNumberLabel.frame = CGRect.init(origin: CGPoint.init(x: xPos, y: 8), size: CGSize.init(width: 60, height: cellHeight / 2))
    }

}
