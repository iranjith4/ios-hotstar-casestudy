//
//  HSBoardDisplayView.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class HSBoardDisplayView: UIView {
    
    var titleLabel = UILabel()
    var dataLabel = UILabel()
    
    override var frame: CGRect {
        didSet {
            updateView()
        }
    }
    
    init() {
        super.init(frame: CGRect.init())
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView() {
        //All the custom initialisation goes here
        
        self.backgroundColor = UIColor.white
        
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        titleLabel.textColor = UIColor.themeGreen
        
        dataLabel.textColor = UIColor.themeGray
        dataLabel.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        dataLabel.textAlignment = .center
        
        self.addSubview(titleLabel)
        self.addSubview(dataLabel)
        updateView()
    }
    
    func updateView() {
        let xPos : CGFloat = 8
        var yPos : CGFloat = 8
        
        titleLabel.frame = CGRect.init(origin: CGPoint.init(x: xPos, y: yPos), size: CGSize.init(width: self.frame.size.width - 2 * xPos, height: 40))
        
        yPos += titleLabel.frame.size.height + 8
        
        dataLabel.frame = CGRect.init(origin: CGPoint.init(x: xPos, y: yPos), size: CGSize.init(width: self.frame.size.width - 2 * xPos, height: self.frame.size.height - 8 - yPos))
    }

    

}
