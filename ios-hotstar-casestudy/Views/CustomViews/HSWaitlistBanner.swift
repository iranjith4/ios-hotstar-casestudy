//
//  HSWaitlistBanner.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class HSWaitlistBanner: UIView {
    
    let totalTables = HSBoardDisplayView()
    let occupiedTables = HSBoardDisplayView()
    let inQueue = HSBoardDisplayView()
    
    override var frame: CGRect {
        didSet {
            updateView()
        }
    }
    
    init() {
        super.init(frame: CGRect.init())
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        //All the custom initialisation goes here
        
        self.backgroundColor = #colorLiteral(red: 0.9606800675, green: 0.9608443379, blue: 0.9606696963, alpha: 1)
        
        totalTables.titleLabel.text = "Total Tables"
        occupiedTables.titleLabel.text = "Occupied Tables"
        inQueue.titleLabel.text = "In Queue"
        
        addSubview(totalTables)
        addSubview(occupiedTables)
        addSubview(inQueue)
        
        updateView()
    }
    
    func updateView() {
        let space : CGFloat = 3
        let width = (self.frame.size.width - 2 * space ) / 3
        let height = self.frame.size.height - 2 * space
        
        var xPos : CGFloat = 0
        totalTables.frame = CGRect.init(origin: CGPoint.init(x: xPos, y: space), size: CGSize.init(width: width, height: height))
        xPos += width + space
        
        occupiedTables.frame = CGRect.init(origin: CGPoint.init(x: xPos, y: space), size: CGSize.init(width: width, height: height))
        xPos += width + space
        
        inQueue.frame = CGRect.init(origin: CGPoint.init(x: xPos, y: space), size: CGSize.init(width: width, height: height))
    }

}
