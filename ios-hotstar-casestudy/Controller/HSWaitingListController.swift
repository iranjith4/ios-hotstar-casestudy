//
//  HSWaitingListController.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class HSWaitingListController: UIViewController {
    
    let waitlistView = HSWaitlistView()
    let waitlistViewModel = HSWaitlistModel()
    
    var totalBarHeight = UIApplication.shared.statusBarFrame.size.height

    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
        self.title = "Waiting List"
        waitlistViewModel.loadCustomers {
            self.updateData()
        }
    }
    
    
    func setupController() {
        //Adding Controller Buttons
        HSUtility.getNavigationButtonWith(buttonTitle : "Logout") { (logoutButtonItem, logoutButton) in
            self.navigationItem.leftBarButtonItem = logoutButtonItem
            logoutButton.addTarget(self, action: #selector(logoutTapped), for: .touchUpInside)
        }
        
        HSUtility.getNavigationButtonWith(buttonTitle: "Add") { (addButtonItem, addButton) in
            self.navigationItem.rightBarButtonItem = addButtonItem
            addButton.addTarget(self, action: #selector(addTapped), for: .touchUpInside)
        }
        
        self.edgesForExtendedLayout = []
        if let navHeight = self.navigationController?.navigationBar.frame.size.height {
            totalBarHeight += navHeight
        }
        
        waitlistView.frame = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: self.view.frame.size.width, height: self.view.frame.size.height - totalBarHeight))
        waitlistView.waitlistTable.dataSource = self
        waitlistView.waitlistTable.delegate = self
        self.view.addSubview(waitlistView)
        
        waitlistViewModel.loadCustomers {
            self.updateData()
        }
    }
    
    func updateData() {
        //Update data from Model
        self.waitlistView.waitlistTable.reloadData()
        self.waitlistView.waitlistBanner.totalTables.dataLabel.text = waitlistViewModel.totalNumberOfTables()
        self.waitlistView.waitlistBanner.occupiedTables.dataLabel.text = waitlistViewModel.occupiedTables()
        self.waitlistView.waitlistBanner.inQueue.dataLabel.text = waitlistViewModel.waitingQueueCount()
    }
    
    @objc func logoutTapped() {
        let alert = UIAlertController.init(title: "Logout ?", message: "Are you sure want to logout ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Logout", style: .destructive, handler: { (action) in
            HSUtility.shared.clearAllUD()
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: HSUtility.NotificationKey.logout.rawValue)))
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func addTapped() {
        let addQueue = HSUtility.getStoryBoard(forId: "HSAddQueueController")
        self.present(addQueue, animated: true, completion: nil)
    }
}

extension HSWaitingListController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return waitlistViewModel.numberOfRows(forSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HSCustomerCell.reuseIdentifier, for: indexPath) as! HSCustomerCell
        if let customer = waitlistViewModel.getCustomer(forIndexPath: indexPath) {
            cell.customerName.text = customer.name != nil ? customer.name : ""
            cell.customerPhone.text = customer.phone != nil ? customer.phone : ""
            cell.numberOfPeopleLabel.text = "\(customer.numberOfGuests) People"
            if indexPath.section == 0 {
                cell.tableNumberLabel.text = "\(customer.table)"
                print(customer.table)
            }else {
                cell.tableNumberLabel.text = "\(indexPath.row + 1)"
            }
        }
        cell.updateCell()
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return waitlistViewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return waitlistViewModel.title(forSection: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return HSCustomerCell._cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 {
            let alert = UIAlertController.init(title: "Sure?", message: "Are you sure want to mark this customer finished ?", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction.init(title: "Yes", style: .destructive, handler: { (action) in
                self.waitlistViewModel.customerFinished(at: indexPath.row, completion: {
                    self.updateData()
                })
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else if indexPath.section == 1 && indexPath.row == 0 {
            
            //TODO: Make a common function to handle similar alerts.
            let alert = UIAlertController.init(title: "Sure?", message: "Are you to allocate Table for this customer ?", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction.init(title: "Yes", style: .default, handler: { (action) in
                self.waitlistViewModel.processQueue(completion: { (isTableFilled) in
                    if isTableFilled {
                        HSUtility.showAlert(withTitle: "Oops", message: "All the tables are filled.", showOnController: self)
                    }else {
                        self.updateData()
                    }
                })
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else {
            HSUtility.showAlert(withTitle: "Error", message: "You can allocate table for the first customer only.", showOnController: self)
        }
    }
    
}
