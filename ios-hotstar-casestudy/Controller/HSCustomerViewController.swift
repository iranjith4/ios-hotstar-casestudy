//
//  HSCustomerViewController.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 01/03/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class HSCustomerViewController: UIViewController {

    @IBOutlet var waitlistViewModel: HSWaitlistModel!
    @IBOutlet var queueNumer: HSBoardDisplayView!
    @IBOutlet var averageWaitingTime: HSBoardDisplayView!
    @IBOutlet var customerName: UILabel!
    
    var customer : Customer! {
        didSet {
            updateData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Wait Time"
        self.navigationController?.navigationItem.hidesBackButton = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        queueNumer.titleLabel.text = "Queue Number"
        averageWaitingTime.titleLabel.text = "Avg Waiting Time in Mins"
        
        HSUtility.getNavigationButtonWith(buttonTitle : "Logout") { (logoutButtonItem, logoutButton) in
            self.navigationItem.leftBarButtonItem = logoutButtonItem
            logoutButton.addTarget(self, action: #selector(logoutTapped), for: .touchUpInside)
        }
        
        waitlistViewModel.loadCustomers {
            updateData()
        }
    }
    
    func updateData() {
        if self.customer == nil || queueNumer == nil || averageWaitingTime == nil || customerName == nil {
            return
        }
        
        let queueNumber = self.waitlistViewModel.getQueueNumber(forCustomer: self.customer) + 1
        queueNumer.dataLabel.text = "\(queueNumber)"
        averageWaitingTime.dataLabel.text = "\(queueNumber * 10)"
        customerName.text = customer.name
    }
    
    @objc func logoutTapped() {
        let alert = UIAlertController.init(title: "Logout ?", message: "Are you sure want to logout ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Logout", style: .destructive, handler: { (action) in
            HSUtility.shared.clearAllUD()
            NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: HSUtility.NotificationKey.logout.rawValue)))
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
