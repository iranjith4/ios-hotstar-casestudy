//
//  HSLoginController.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit
import FileProvider

class HSLoginController: UIViewController {
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    
    @IBOutlet var loginViewModel: HSLoginModel!
    @IBOutlet var userTypeSegment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
        self.title = "Login"
    }
    
    func setupController() {
        self.view.backgroundColor = UIColor.white
        
        self.loginButton.layer.cornerRadius = 6
        
        //Adding Tapgesture to dismiss keyboard on tap
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
        userTypeSegmentChanged()
    }
    
    @objc func dismissKeyboard() {
        if usernameField.isFirstResponder {
            usernameField.resignFirstResponder()
        } else if passwordField.isFirstResponder {
            passwordField.resignFirstResponder()
        }
    }
    
    @IBAction func loginTapped() {
        
        guard let _username = usernameField.text, usernameField.text != "" else {
            return
        }
        
        guard let _password = passwordField.text, passwordField.text != "" else {
            return
        }
        
        switch loginViewModel.userType {
        case .customer :
            guard let customer = loginViewModel.getCustomer(forPhoneNumber: _username) else {
                HSUtility.showAlert(withTitle: "Error", message: "User with this phone number doen't waiting.", showOnController: self)
                return
            }
            if loginViewModel.authenticateCustomer(withUserName: _username, andPassword: _password) {
                HSUtility.shared.saveUDBoolData(value: true, for: HSUtility.UserDefaultsKey.isLoggedIn.rawValue)
                HSUtility.shared.saveUDIntData(value: 1, for: HSUtility.UserDefaultsKey.userType.rawValue)
                HSUtility.shared.saveUDStringData(value: _username, for: HSUtility.UserDefaultsKey.customerId.rawValue)
                prepareForLogin()
                let customerController = HSUtility.getStoryBoard(forId: HSUtility.StoryboardId.customerController.rawValue) as! HSCustomerViewController
                customerController.customer = customer
                self.navigationController?.pushViewController(customerController, animated: true)
            }else {
                HSUtility.showAlert(withTitle: "Error", message: "Phonenumber and password doesn't match. Please try again.", showOnController: self)
            }
        case .receptionist :
            if loginViewModel.authenticateLogin(withUserName: _username, andPassword: _password) {
                HSUtility.shared.saveUDBoolData(value: true, for: HSUtility.UserDefaultsKey.isLoggedIn.rawValue)
                HSUtility.shared.saveUDIntData(value: 0, for: HSUtility.UserDefaultsKey.userType.rawValue)
                prepareForLogin()
                let waitingListController = HSUtility.getStoryBoard(forId: HSUtility.StoryboardId.waitingListController.rawValue)
                self.navigationController?.pushViewController(waitingListController, animated: true)
            }else {
                HSUtility.showAlert(withTitle: "Error", message: "Username and password doesn't match. Please try again.", showOnController: self)
            }
        }
    }
    
    func prepareForLogin() {
        self.usernameField.text = ""
        self.passwordField.text = ""
        dismissKeyboard()
    }
    
    @IBAction func userTypeSegmentChanged() {
        if userTypeSegment.selectedSegmentIndex == 0 {
            loginViewModel.userType = .customer
            usernameField.placeholder = "Phone Number"
        }else if userTypeSegment.selectedSegmentIndex == 1 {
            loginViewModel.userType = .receptionist
            usernameField.placeholder = "Username"
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
