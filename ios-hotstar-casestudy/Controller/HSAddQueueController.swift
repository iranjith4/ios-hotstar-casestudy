//
//  HSAddQueueController.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class HSAddQueueController: UIViewController {
    @IBOutlet var name: UITextField!
    @IBOutlet var phone: UITextField!
    @IBOutlet var numberOfGuestsLabel: UILabel!
    var numberOfGuests : Int = 1 {
        didSet {
            numberOfGuestsLabel.text = "\(numberOfGuests)"
        }
    }
    
    @IBOutlet var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupController()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupController() {
        addButton.layer.cornerRadius = 6
        
        //Adding Tapgesture to dismiss keyboard on tap
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func dismissKeyboard() {
        if name.isFirstResponder {
            name.resignFirstResponder()
        }else if phone.isFirstResponder {
            phone.resignFirstResponder()
        }
    }
    
    @IBAction func cancelTapped() {
        if self.name.isFirstResponder {
            self.name.resignFirstResponder()
        }else if self.phone.isFirstResponder {
            self.phone.resignFirstResponder()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTapped() {
        guard let _name = name.text, name.text != "" else {
            HSUtility.showAlert(withTitle: "Missed Details", message: "Please fill the name.", showOnController: self)
            return
        }
        
        guard let _phone = phone.text, phone.text?.count == 10 else {
            HSUtility.showAlert(withTitle: "Missed Details", message: "Please Check the phone number.", showOnController: self)
            return
        }
        
        guard HSCoreDataManager().doesThisUserExist(phoneNumber: _phone) == false else {
            HSUtility.showAlert(withTitle: "Duplicate", message: "User already in Queue.", showOnController: self)
            return
        }
        
        let customerData = [
            "name" : _name,
            "numberOfGuests" : numberOfGuests,
            "status" : 0,
            "phone" : _phone,
            "table" : 0
        ] as [String : Any]
        
        HSCoreDataManager().saveAddedCustomer(customer: customerData)
        
        self.cancelTapped()
    }
    
    @IBAction func numberOfGuestsChanged(_ sender: UIStepper) {
        if sender.value > 10 {
            sender.value = 10
        }
        
        self.numberOfGuests = Int(sender.value)
        
    }
    

}
