//
//  ViewController.swift
//  ios-hotstar-casestudy
//
//  Created by ranjith on 28/02/18.
//  Copyright © 2018 ir4. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setupController() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(logout), name: NSNotification.Name(rawValue: HSUtility.NotificationKey.logout.rawValue), object: nil)
        
        checkForLogin()
    }
    
    func checkForLogin() {
        if isUserLoggedIn() {
            //Checklogin type
            if let loginType = HSUtility.shared.getUDIntData(for: HSUtility.UserDefaultsKey.userType.rawValue) {
                if loginType == 0 {
                    let waitListController = HSUtility.getStoryBoard(forId: HSUtility.StoryboardId.waitingListController.rawValue)
                    self.navigationController?.pushViewController(waitListController, animated: false)
                    return
                }else if loginType == 1 {
                    let viewModel = HSLoginModel()
                    if let phoneNumber = HSUtility.shared.getUDStringData(for: HSUtility.UserDefaultsKey.customerId.rawValue), let customer = viewModel.getCustomer(forPhoneNumber: phoneNumber) {
                        let customerController =  HSUtility.getStoryBoard(forId: HSUtility.StoryboardId.customerController.rawValue) as! HSCustomerViewController
                        self.navigationController?.pushViewController(customerController, animated: false)
                        customerController.customer = customer
                        return
                    }
                }
            }
        }
        let loginController = HSUtility.getStoryBoard(forId: HSUtility.StoryboardId.loginController.rawValue)
        self.navigationController?.pushViewController(loginController, animated: false)
    }
    
    func isUserLoggedIn() -> Bool{
        if let isLoggedIn = HSUtility.shared.getUDBoolData(for: HSUtility.UserDefaultsKey.isLoggedIn.rawValue) {
            return isLoggedIn
        }
        return false
    }
    
    @objc func logout() {
        self.navigationController?.popToViewController(self, animated: false)
        checkForLogin()
    }


}

